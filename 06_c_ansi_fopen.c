/* Standard ANSI C */
#include <stdio.h>

int main(int argc, char const *argv[]) {
  FILE *source = fopen("origen", "r");
  FILE *destination = fopen("destino", "w");
  char buffer;

  while (!feof(source)) {
    buffer=fgetc(source);
    if(feof(source)) break;
    fputc(buffer, destination);
  } 

  fclose(destination);
  fclose(source);
  return 0;
}
