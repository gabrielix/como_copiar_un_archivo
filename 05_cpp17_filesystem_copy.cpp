#include <iostream>
#include <filesystem>

int main(int argc, char const *argv[])
{
    if(std::filesystem::copy_file("origen", "destino")) {
        return 0;
    } else {
        return 1;
    }
}

// clang 14 or later
// clang++ -std=c++17 
// else error 
