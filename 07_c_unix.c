/* Standard UNIX C */
//#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char const *argv[]) {
  int source = open("origen", O_RDONLY);
  int destination = open("destino", O_WRONLY | O_CREAT, 0644);
  char buffer[512];
  ssize_t readed;

  do {
    readed=read(source, buffer, 512); 
    write(destination, buffer, readed);
  } while(readed);

  close(destination);
  close(source);
  return 0;
}
